<?php
/**
 * @file
 * uw_cfg_conference_overrides.features.inc
 */

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_cfg_conference_overrides_field_default_field_instances_alter(&$data) {
  if (isset($data['node-uw_blog-field_audience'])) {
    unset($data['node-uw_blog-field_audience']);
  }
  if (isset($data['node-uw_blog-field_podcast_transcript'])) {
    unset($data['node-uw_blog-field_podcast_transcript']);
  }
  if (isset($data['node-uw_blog-field_podcast_url'])) {
    unset($data['node-uw_blog-field_podcast_url']);
  }
  if (isset($data['node-uw_blog-field_tag'])) {
    unset($data['node-uw_blog-field_tag']);
  }
  if (isset($data['node-uw_web_form-field_audience'])) {
    unset($data['node-uw_web_form-field_audience']);
  }
  if (isset($data['node-uw_web_form-field_related_link'])) {
    unset($data['node-uw_web_form-field_related_link']);
  }
  if (isset($data['node-uw_web_form-field_sidebar_content'])) {
    unset($data['node-uw_web_form-field_sidebar_content']);
  }
  if (isset($data['node-uw_web_page-field_audience'])) {
    unset($data['node-uw_web_page-field_audience']);
  }
  if (isset($data['node-uw_web_page-field_related_link'])) {
    unset($data['node-uw_web_page-field_related_link']);
  }
  if (isset($data['node-uw_web_page-field_sidebar_content'])) {
    unset($data['node-uw_web_page-field_sidebar_content']);
  }
  if (isset($data['node-uw_web_page-field_sidebar_note_content'])) {
    unset($data['node-uw_web_page-field_sidebar_note_content']);
  }
  if (isset($data['node-uw_web_page-field_sidebar_note_content_links'])) {
    unset($data['node-uw_web_page-field_sidebar_note_content_links']);
  }
}
