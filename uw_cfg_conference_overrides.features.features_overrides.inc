<?php
/**
 * @file
 * uw_cfg_conference_overrides.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_conference_overrides_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.node-uw_blog-field_audience.node-uw_blog-field_audience"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_blog-field_podcast_transcript.node-uw_blog-field_podcast_transcript"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_blog-field_podcast_url.node-uw_blog-field_podcast_url"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_blog-field_tag.node-uw_blog-field_tag"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_form-field_audience.node-uw_web_form-field_audience"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_form-field_related_link.node-uw_web_form-field_related_link"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_form-field_sidebar_content.node-uw_web_form-field_sidebar_content"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_audience.node-uw_web_page-field_audience"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_related_link.node-uw_web_page-field_related_link"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_sidebar_content.node-uw_web_page-field_sidebar_content"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_sidebar_note_content.node-uw_web_page-field_sidebar_note_content"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_web_page-field_sidebar_note_content_links.node-uw_web_page-field_sidebar_note_content_links"]["DELETED"] = TRUE;

 return $overrides;
}
